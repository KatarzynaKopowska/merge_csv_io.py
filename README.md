
This is second of my solutions for an internship task.
The goal was to create a function that merge selected data from two .csv files,
returning a list of dictionaries.
I use csv library to read .csv files and io library to create reader objects. 
The best solution for this internship task was to create StringIO objects
as arguments for merge functions.

The result of the function should look as follows:

[{
'id': (id number), 
'name': (persons name),
'surname': (persons surname),
'visits': (number of visits)
}]
