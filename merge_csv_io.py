import csv
import io

def merge(persons_file, visits_file):
    persons_file_data = []
    persons = persons_file.read()
    persons_reader = csv.reader(io.StringIO(persons), delimiter=",")
    
    for data in persons_reader:
        persons_file_data.append(data)

    visits_file_data = []
    visits = visits_file.read()
    visits_reader = csv.reader(io.StringIO(visits), delimiter=",")
    
    for data in visits_reader:
        visits_file_data.append(data)

    id_list = [(persons_file_data[i][0]) for i, num in enumerate(persons_file_data)]
    id_list = id_list[1:]
    
    persons_id_list = [(visits_file_data[k][1]) for k, num in enumerate(visits_file_data)]

    number_of_visits = []
    for id_number in (id_list):
        counter = persons_id_list.count(id_number)
        number_of_visits.append(counter)

    final_list = []
    index_num = []
    person_data = persons_file_data[1:]
    for index_num in range(len(id_list)):
        final_dict = {}
        final_dict["id"] = id_list[index_num]
        final_dict["name"] = person_data[index_num][1]
        final_dict["surname"] = person_data[index_num][2]
        final_dict["visits"] = number_of_visits[index_num]
        index_num+=1
        final_list.append(final_dict)

    return final_list

person = io.open('C:\\anaconda_scripts\\persons_file.csv', 'r')
visits = io.open('C:\\anaconda_scripts\\visits_file.csv', 'r')

merge(person,visits)
